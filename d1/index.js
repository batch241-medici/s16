// alert("Hello, Batch 241");

// Arithmetic Operators
let x = 1397;
let y = 7831;

// Addition Operator
let sum = x + y;
console.log("Result of addition operator: " + sum);

// Subtraction Operator
let difference = x - y;
console.log("Result of subraction operator: " + difference);

// Multiplication Operator
let product = x * y;
console.log("Result of multiplication opearator: " + product);

// Division Operator
let quotient = x / y;
console.log("Result of division opearator: " + quotient);

// Modulo Operator
let remainder = y % x;
console.log("Result of modulo operator: " + remainder);

// Assignment Operator
// Basic Assignment Operator (=)
// The assignment operator adds the value of the right operand to a variable and assigns the results to the variable
let assignmentNumber = 8;

// Addition Assignment Operator
// Uses the current value of the variable and it ADDS a number (2) to itself. Afterwards, it reassigns it as a  new value.
// assignmentNumber = assignmentNumber + 2
assignmentNumber += 2;
console.log("Result of the addition assignment operator: " + assignmentNumber); //10

// Subraction/Multiplcation/Division (-=, *=, /=)

// Subraction Assignment Operator
assignmentNumber -= 2;
console.log("Result of the subraction assignment operator: " + assignmentNumber); //8

// Multiplication Assignment Operator
assignmentNumber *= 2;
console.log("Result of the multiplication assignment operator: " + assignmentNumber); //16

// Division Assignment Operator
assignmentNumber /= 2;
console.log("Result of the division assignment operator: " + assignmentNumber); //8

// Multiple Operators and Parentheses
/*
	When multiple operators are applied in a single statement, it follows the PEMDAS Rule (Parentheses, Exponents, Multiplication, Division, Addition, Subtraction)
	1. 3 * 4 = 12
	2. 12 / 5 = 2.4
	3. 1 + 2 = 3
	4. 3 - 2.4 = 0.6
*/
// let mdas = 3 - 2.4;
let mdas = 1 +2 - 3 * 4 / 5;
console.log("Result of mdas operation: " + mdas);

// The order of operations can be changed by adding parenthesis to the logic.
let pemdas = 1 + (2 - 3) * (4 / 5);
console.log("Result of pemdas operation: " + pemdas);

let sample = 5 % 2 + 10;
console.log(sample);

sample = 5 + 2 % 10;
console.log(sample);

// Increment and Decrement
// Operators that add or subract values by 1 and reassigns the value of the variable where the increment/decrement was applied to

let z = 1;

// let increment = z;
// console.log(increment);

// Pre-Increment
// The value of "z" is added by a value of 1 before returning the value and storing it in the variable "increment";
/*let increment = ++z;
console.log("Result of the pre-increment: " + increment);
console.log("Result of the pre-increment: " + z);*/

// Post-Increment
// The value of "z" is returned and stored in the varialbe "postIncrement" then the value of "z" is increased by one
let postIncrement = z++;
console.log("Result of the post-increment: " + postIncrement);
console.log("Result of the postIncrement: " + z);

/*
	Pre-increment - adds 1 first before reading value
	Post-increment - reads value first before adding 1
*/

let a = 2;

// Pre-Decrement
// The value "a" is decreased by a value of 1 before returning the value and storing it in the variable "preDecrement"
/*let preDecrement = --a;
console.log("Result of the pre-decrement: " + preDecrement);
console.log("Result of the pre-decrement: " + a);*/

// Post-Decrement
// The value of "a" is returned and stored in teh varaible "postDecrement" then the value of "a" is decreased by 1;
let postDecrement = a--;
console.log("Result of the postDecrement: "+ postDecrement);
console.log("Result of the postDecrement: " + a);

// Type Coercion
/*
	Type coercion is the automatic or implicit conversion of values from one data type to another
*/

let numA = '10';
let numB = 12;

/*
	Adding/concatenating a string and a number will result as a string
*/

let coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion);

let coercion1 = numA - numB;
console.log(coercion1);
console.log(typeof coercion1);

// Non-coercion
let numC = 16;
let numD = 14;

let nonCoercion = numC + numD;
console.log(nonCoercion);
console.log(typeof nonCoercion);

// Addition of Number and Boolean
/*
	The result is a number
	The boolean "true" is associated with the value of 1 
	The boolean "false" is associated with the value of 0
*/

let numE = true + 1;
console.log(numE);
console.log(typeof numE);

let numF = false + 1;
console.log(numF);

// Comparison Operator
let juan = 'juan';

// Equality Operator (==)
/*
	- Checks whether the operands are equal/have the same content
	- Attempts to CONVERT and COMPARE operands of different data types
	- Returns a boolean value
*/

console.log(1 == 1); //True
console.log(1 == 2); //False
console.log(1 == '1'); //True
console.log(1 == true); //True
// compare two strings that are the same
console.log('juan' == 'juan'); //True
console.log('true' == true); //False
console.log(juan == 'juan'); //True

// Inequality Operator (!=)
/*
	- Checks whether the operands are not equal/have different content
	- Attempts to CONVERT and COMPARE operands of different data types
*/

console.log(1 != 1); //False
console.log(1 != 2); //True
console.log(1 != '1'); //False
console.log(1 != true); //False
console.log('juan' != 'juan'); //False
console.log('juan' != juan); //False

// Strict Equality Operator (===)
/*
	- Checks whether the operands are equal/have the same content
	- Also COMPARES the data types of 2 values
*/

console.log(1 === 1); //True
console.log(1 === 2); //False
console.log(1 === '1'); //False
console.log(1 === true); //False
console.log('juan' === 'juan'); //True
console.log(juan === 'juan'); //True

// Strict Inequality Operator (!==)
/*
	- Checks whether the operands are not equal/have the same content
	- Also COMPARES the data types of 2 values
*/

console.log(1 !== 1); //False
console.log(1 !== 2); //True
console.log(1 !== '1'); //True
console.log(1 !== true); //True
console.log('juan' !== 'juan'); //False
console.log(juan !== 'juan'); //False

// Relational Operator
let j = 50;
let k = 65;

// GT/Greater than operator (>)
let isGreaterThan = j > k;
console.log(isGreaterThan); //False

// LT/Less than operator (<)
let isLessThan = j < k;
console.log(isLessThan); //True

// GTE/Greater than or Equal Operator (>=)
let isGTorEqual = j >= k;
console.log(isGTorEqual); //False

// LTE/Less than or Equal Operator (<=)
let isLTorEqual = j <= k;
console.log(isLTorEqual); //True

let numStr = "30";
// Forced coercion to change the string to a number
console.log(j > numStr); //True

let str = "thirty";
// (50 is greater than NaN)
console.log(j > str); //False

// Logical Operator
let isLegalAge = true;
let isRegistered = false;

// Logical AND Operator (&& - Double Ampersand)
// Returns true if all operands are true
// true && true = true
// true && false = false

let allRequirementsMet = isLegalAge && isRegistered;
console.log(allRequirementsMet); //False

// Logical OR Operator (|| - Double Pipe)
// Returns true if one of the operands are true
// true || true = true
// true || false = true
// false || false = false

let someRequirementsMet = isLegalAge || isRegistered;
console.log(someRequirementsMet); //True

// Logical NOT Operator (! - Exclamation point)
// Returns the opposite value

let someRequirementsNotMet = !isRegistered;
console.log(someRequirementsNotMet); //True

// AND - all should be true ~~ true
// AND - if is false ~~ false
// OR - at least one is true ~~ true
// OR - all are false ~~ false
// (NOT true) !true = false
// (NOT false) !false = true